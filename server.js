const mnsig = require("mnsig-client");
const express = require("express");
const bodyParser = require("body-parser");
const logger = require("morgan");

const PORT = process.env.PORT || 7711;
const HOST = process.env.HOST || "127.0.0.1";

const SERVER_ERROR = {
  isError: true,
  code: 500,
  data: {message: "Failed to handle the request"}
};

const client = new mnsig.MNSigClient();
const app = express();

const handle = async (res, partial) => {
  let result;
  try {
    result = await partial();
  } catch (err) {
    console.error(`failed to execute "${partial.name}"`, err);
    result = SERVER_ERROR;
  }

  // mnsig-client returns negative error codes for local errors,
  // which expressjs forbids.
  res.status(Math.abs(result.code));
  res.json(result);
};

app.use(logger("combined"));
app.use(bodyParser.json({limit: "8mb"}));

app.use((req, res, next) => {
  // Update mnsig client from token and instance headers if passed.
  const token = req.get("token");
  const instance = req.get("instance");
  if (token) {
    client.token = token;
  }
  if (instance) {
    client.instance = instance;
  }
  next();
});

app.post("/config", (req, res) => {
  // Update mnsig client (alternative to passing headers on each request).
  const token = req.body.token;
  const instance = req.body.instance;
  if (token) {
    client.token = token;
  }
  if (instance) {
    client.instance = instance;
  }
  res.json({success: true});
});

/*
app.post("/wallet/", function(req, res) {
  client.createWallet(req.body, function(err, json) {
    callback(res, err, json);
  });
});

app.put("/wallet/", function(req, res) {
  client.addWalletKey(req.body, function(err, json) {
    callback(res, err, json);
  });
});
*/

app.get("/wallet/:wallet", async (req, res) => {
  await handle(res, client.walletInfo.bind(client, req.params));
});

app.get("/wallet/:wallet/keys", async (req, res) => {
  await handle(res, client.walletKeys.bind(client, req.params));
});

app.get("/wallet/:wallet/balance", async (req, res) => {
  let params = { wallet: req.params.wallet };
  if (req.query.target !== undefined) {
    params.target = req.query.target;
  }
  await handle(res, client.balance.bind(client, params));
});

app.post("/wallet/:wallet/address", async (req, res) => {
  req.body.wallet = req.params.wallet;
  await handle(res, client.newAddress.bind(client, req.body));
});

app.get("/wallet/:wallet/transactions", async (req, res) => {
  let params = req.query;
  params.wallet = req.params.wallet;
  await handle(res, client.txlist.bind(client, params));
});

app.get("/wallet/:wallet/transaction", async (req, res) => {
  let params = req.query;
  params.wallet = req.params.wallet;
  await handle(res, client.txget.bind(client, params));
});

app.get("/wallet/:wallet/utxos", async (req, res) => {
  await handle(res, client.listUTXOs.bind(client, req.params));
});

app.get("/wallet/:wallet/estimatefee/:target", async (req, res) => {
  await handle(res, client.estimateFee.bind(client, req.params));
});

app.post("/transaction/proposal/", async (req, res) => {
  const proposal = req.body.proposal;
  await handle(res, client.startProposal.bind(client, proposal));
});

app.put("/transaction/proposal/", async (req, res) => {
  const proposal = req.body.proposal;
  await handle(res, client.prepareProposal.bind(client, proposal));
});

app.post("/transaction/proposal/many", async (req, res) => {
  const proposal = req.body.proposal;
  await handle(res, client.startManyProposal.bind(client, proposal));
});

app.get("/transaction/:wallet/proposal/:id", async (req, res) => {
  await handle(res, client.getProposal.bind(client, req.params));
});

app.delete("/transaction/:wallet/proposal/:id", async (req, res) => {
  await handle(res, client.deleteProposal.bind(client, req.params));
});

app.post("/transaction/proposal/sign", async (req, res) => {
  const proposal = req.body.proposal;
  const xpriv = req.body.xpriv;
  await handle(res, client.localSign.bind(client, proposal, xpriv));
});

app.post("/transaction/broadcast", async (req, res) => {
  const proposal = req.body.proposal;
  await handle(res, client.broadcast.bind(client, proposal));
});

app.get("/transaction/:wallet/:txid", async (req, res) => {
  await handle(res, client.txinfo.bind(client, req.params));
});


const listener = app.listen(PORT, HOST, () => {
  console.log(`Listening on ${JSON.stringify(listener.address())}`);
});
